#!/usr/bin/env python2

# OJOC - An Open JOdel Client
# Copyright (C) 2016  Christian Fibich
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import OJOC.Connection
import json
import os
import signal
import sys
import select
import requests
import datetime
import copy
import json
import dateutil.parser
import dateutil.tz as tz
import time
import argparse
import re

alltime_posts = {}
latest_posts = {}
geodump_prefix = None
lexical_prefix = None


def signal_handler(signum, frame):
    print "Signal caught."
    if geodump_prefix is not None:
        fn = geodump_prefix[0] + '_' + datetime.datetime.now().strftime("%Y-%m-d_%H-%M-%S") + '.dump'
        print "Exporting to " + fn
        export_coordinates(fn, latest_posts)
    if lexical_prefix is not None:
        fn = lexical_prefix[0] + '_' + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + '.dump'
        print "Exporting to " + fn
        export_lexical(fn, latest_posts)

    if (signum == signal.SIGINT):
        sys.exit(1)


def export_coordinates(fn, posts):

    data = {'time': datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}
    data['data'] = []
    for pid in posts.keys():
        item = posts[pid]
        data['data'].append({'lat': item['location']['loc_coordinates']['lat'], 'lon': item['location']['loc_coordinates']['lng'], 'time': item['created_at']})

    with open(fn, 'w') as f:
        json.dump(data, f)


def export_lexical(fn, posts):

    data = {'time': datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}
    words = {}
    for pid in posts.keys():
        item = posts[pid]
        if ('image_url' in item.keys()):
            continue
        post_words = re.sub(r'[^# \n\w]+', '', item['message'].lower(), flags=re.UNICODE).split(" ")
        for word in post_words:
            word = re.sub(r'\s+', '', word, flags=re.UNICODE)
            if word in words.keys():
                words[word] += 1
            else:
                words[word] = 1

    data['data'] = words

    with open(fn, 'w') as f:
        json.dump(data, f)


def store_post(post, posts, earliest):

    pi = post['post_id']
    tm = post['created_at']

    try:
        ts = dateutil.parser.parse(tm)
    except Exception as e:
        print "Invalid timestamp: " + tm
        return posts

    if (ts > earliest):
        posts[pi] = post

    return posts


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--geodump', nargs=1, help='Dump geocoordinates to file <prefix>_<timestamp>.dump', metavar='PREFIX')
    parser.add_argument('-l', '--lexical-analysis', nargs=1, help='Dump lexical analysis to file <prefix>_<timestamp>.dump', metavar='PREFIX')
    parser.add_argument('-d', '--dump-duration', nargs=1, help='Duration before saving in seconds', default=3600, type=int, metavar='DURATION')
    parser.add_argument('-r', '--refresh-duration', nargs=1, help='Duration before refreshing in seconds', default=10, type=int, metavar='DURATION')
    parser.add_argument('-c', '--citycc', nargs=1, help='Your location, e.g. Vienna,AT', metavar='CITY,CC')

    print """
    OJOC, Copyright (C) 2016 Christian Fibich
    OJOC comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.
    This is free software, and you are welcome to redistribute it
    under certain conditions; see LICENSE for details.
    """

    try:
        n = parser.parse_args(sys.argv[1:])
        args = vars(n)
    except:
        sys.exit(1)

    loc = args.get('citycc')

    if (loc is not None):
        print "Location specified as " + loc[0]
        loc = loc[0]

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGUSR1, signal_handler)

    geodump_prefix = args.get('geodump')
    lexical_prefix = args.get('lexical_analysis')

    if(geodump_prefix is None and lexical_prefix is None):
        print "At least one of --geodump or --lexical-analysis is required."
        parser.print_help()
        sys.exit(1)

    if (isinstance(args['dump_duration'], list)):
        dump_duration = args['dump_duration'][0]
    else:
        dump_duration = args['dump_duration']

    if (isinstance(args['refresh_duration'], list)):
        refresh_duration = args['refresh_duration'][0]
    else:
        refresh_duration = args['refresh_duration']

    conn = OJOC.Connection.Connection(citycc=loc)

    cont = True
    start = tss = datetime.datetime.now(tz.tzlocal())

    while cont:
        print "Retrieving..."
        print "Location: " + str(conn.location)

        new_post_data = conn.recent_posts()

        try:
            for post in new_post_data['posts']:
                if (post is None):
                    continue
                latest_posts = store_post(post, latest_posts, tss)
                children = post.get('children')
                if (children is None):
                    continue
                for child in children:
                    latest_posts = store_post(child, latest_posts, tss)
        except Exception as e:
            print "Error: " + str(e)
            continue

        print "Runtime: " + str(datetime.datetime.now(tz.tzlocal()) - start)
        if (datetime.datetime.now(tz.tzlocal()) - tss > datetime.timedelta(seconds=dump_duration)):
            tss = datetime.datetime.now(tz.tzlocal())
            if geodump_prefix is not None:
                fn = geodump_prefix[0] + '_' + datetime.datetime.now(tz.tzlocal()).strftime("%Y-%m-%d_%H-%M-%S") + '.dump'
                export_coordinates(fn, latest_posts)
            if lexical_prefix is not None:
                fn = lexical_prefix[0] + '_' + datetime.datetime.now(tz.tzlocal()).strftime("%Y-%m-%d_%H-%M-%S") + '.dump'
                export_lexical(fn, latest_posts)

            latest_posts = {}

        time.sleep(refresh_duration)
