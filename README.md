# README #

OJOC (Open JOdel Client) is an *inofficial* proof-of-concept implementation of a Python Client for tellm/Jodel.

I have no affiliation with _The Jodel Venture GmbH_.
Get the official app in the PlayStore or AppStore.

## Screenshots ##

### OJOC ###

![OJOC](docs/ojoc.png)

### NOJOC ###

![NOJOC](docs/nojoc.png)

using [cool-retro-term](https://github.com/Swordfish90/cool-retro-term)

![NOJOC-CRT](docs/nojoc-crt.png)

## Prerequisites ##

* Python
* `python-gi`
* `python-requests`
* `python-appdirs`
* `python-shutil`
* `python-pillow`
* `python-enum34` to avoid "TypeError: 'type' object is not iterable" (Ty. Dominic S)
* `python-urwid`
* WebKit, WebKit-GTK (e.g., `gir1.2-webkit-3.0` under Debian)


Under Windows, install _PyGObject_ from https://sourceforge.net/projects/pygobjectwin32/

Under OSX, install _brew_ from 
http://brew.sh/

And then:

* `$ brew install python`
* `$ brew install gtk+3`
* `$ brew install adwaita-icon-theme`
* `$ brew install pygobject3`
* `$ pip install pillow`
* `$ pip install python-dateutil`
* `$ pip install keyring`
* `$ pip install python-enum`
* `$ pip install python-urwid`

For Emoji support, you need to install the Unicode Emoji symbols, e.g. via the _Symbola_ font.

* Under Debian, this is available via the package `ttf-ancient-fonts-symbola`
* Under Windows, get Symbola, e.g., from http://users.teilar.gr/~g1951d/Symbola.zip
* For OSX: no solution yet...

## OS Support ##

GUI Code successfully tested under

* Debian Testing, Python 2.7.11
* Windows 7 x64, Python 2.7.11
* OSX 10.10.5, With Python 2.7.11 from brew

Command-line tools also successfully ran under

* Raspbian Jessie

## Getting started ##

### Linux ###

Just run from any terminal:

`./ojoc.py`

### Windows ###

Just double-click on `ojoc.py` and hope for the best :stuck_out_tongue:

### OSX ###

Install Python via brew and all the packages mentioned above,
then run `python ojoc.py` in a terminal window.

## Command-line Stuff ##

Although `ojoc.py` is a shiny GUI app, it supports the following command line options:

    usage: ojoc.py [-h] [-c CITY,CC]

      -h, --help            show this help message and exit
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`nojoc.py` is a _Urwid_ based terminal friendly text app. Jodel over SSH!

    usage: ojoc.py [-h] [-c CITY,CC]

      -h, --help            show this help message and exit
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_dumper.py` is a command-line tool to extract location and lexical dumps for
statistical analysis.

It can be called like this:

    At least one of --geodump or --lexical-analysis is required.
    usage: ojoc_dumper.py [-h] [-g PREFIX] [-l PREFIX] [-d DURATION] [-r DURATION]
                          [-c CITY,CC]

    optional arguments:
      -h, --help            show this help message and exit
      -g PREFIX, --geodump PREFIX
                            Dump geocoordinates to file <prefix>_<timestamp>.dump
      -l PREFIX, --lexical-analysis PREFIX
                            Dump lexical analysis to file
                            <prefix>_<timestamp>.dump
      -d DURATION, --dump-duration DURATION
                            Duration before saving in seconds
      -r DURATION, --refresh-duration DURATION
                            Duration before refreshing in seconds
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_imgdump_fast.py` is a command-line tool for dumping all current images in the "recent" section.

It can be called like this:

    usage: ojoc_imgdump_fast.py [-h] [-r DURATION] [-l] [-o PATH] [-c CITY,CC]

    optional arguments:
      -h, --help            show this help message and exit
      -r DURATION, --refresh-duration DURATION
                            Duration before refreshing in seconds
      -l, --loop            Run in loop
      -o PATH, --output-dir PATH
                            Where to put the images
      -c CITY,CC, --citycc CITY,CC
                            Your location, e.g. Vienna,AT

`ojoc_imgdump.py` fulfills the same purpose, but keeps a state file and
tries to follow all seen posts with initial images.


## Hacky Hacker Stuff ##

The tellm Rest API is reachable via `https://api.go-tellm.com/api/v2/`.

It uses an SHA256 Hash over a Device ID as a user identifier (`device_uid`).

When OJOC is launched the first time, it generates a 256-bit random number from `os.urandom`
and uses this number as identifier to get an Access Token for *Bearer* Authorization.
The `device_uid` is stored in the OS' keyring using `keyring.set_password` under the
service *jodel* as *device_uid*

Once OJOC receives a valid Access Token, it stores it in `~/.jodel/jodel.cfg`

Before every request, OJOC checks if the Token is still valid, and requests a new one
if necessary.

### Signing ###

#### Since Version 4.19.1 ####

With Version 4.19.1, Jodel switched to a more complicated
way of signing requests than described below.

The native libhmac.so now contains three functions exported
to Java:

- `void init(void)`

- `void register(String s)`

- `String sign(String s1, String s2, Byte []b)`

Apparently (from the decompiled APK), `init()` must be called once,
and then for each API request:

1. `register(<HTTP METHOD>@</api/path>)`

2. `sign(<APK Signature Hash>,<HTTP METHOD>@</api/path>,<Stringified Request>)`

Aparently (reverse engineering work by @jangarcia, see discussion
of #14), the big change from 4.19.1 to 4.20.1 was that the HMAC
secret is now more thoroughly obfuscated.

Apart from that, signing works just as with 4.19.1.

There exists a fallback key in the `libhmac.so` which is not encrypted
but does not work with the production API (maybe some kind of developer
key?)

Keys may be retrieved from the `libhmac.so` using `ojoc-x86-keyhack`.

#### Before Version 4.19.1 ####

A HMAC hash is currently required for 

* requesting an initial access token
* posting images

This hash is generated by the app as follows:

* Calculate SHA256 hash of APK signature
* Feed hash into native library
* Library function outputs a HMAC secret
* Use this secret to calculate HMAC-SHA1 hash
* Add hash as HTTP header to requests

As the HMAC secret output by the library does not change over time,
we can store it once as done in OJOC/Config.py. This will likely change
in the following App releases.

The HMAC secret can be retrieved from the MIPS libhmac.so by the
method described in ojoc-mips-keyhack, and needs to be updated in
OJOC/Config.py for each new Jodel Android release.


## ToDo ##

* Keep up with HMAC Key changes
* Code Refactoring
* Adding additional features
* Keep up with API changes