#ifndef __decrypt_h__
#define __decrypt_h__

#define KEY_LOCATION_START              0xFC00
#define KEY_LOCATION_HAYSTACK_SIZE      0x1000
#define CLIENT_SECRET_SIZE                  40
#define CRYPTTABLE_SIZE                    256
#define SIGNATURE "a4a8d4d7b09736a0f65596a868cc6fd620920fb0"

int decode (const uint8_t xorKey[CLIENT_SECRET_SIZE]);

#endif /* __decrypt_h__ */
